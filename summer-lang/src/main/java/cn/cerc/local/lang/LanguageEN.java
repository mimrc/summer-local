package cn.cerc.local.lang;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import cn.cerc.db.core.Datetime;
import cn.cerc.db.core.ILanguage;
import cn.cerc.db.core.Lang;
import cn.cerc.db.core.MD5;
import cn.cerc.ui.translate.KnowallTranslateAs;
import cn.cerc.ui.translate.KnowallTranslateGet;
import cn.cerc.ui.translate.KnowallTranslateLoad;
import cn.cerc.ui.translate.LanguageText;

@Component
public class LanguageEN implements ILanguage {
    private static final Logger log = LoggerFactory.getLogger(LanguageEN.class);
    private final Map<String, LanguageText> buffer = new ConcurrentHashMap<>();
    private final ExecutorService executor = Executors.newVirtualThreadPerTaskExecutor();
    private final Map<String, Set<String>> asMap = new ConcurrentHashMap<>();
    private final Map<String, Map<String, String>> getMap = new ConcurrentHashMap<>();
    private final ReentrantLock lock = new ReentrantLock();

    public LanguageEN() {
        readFromFile("/summer-db-en.properties");
        readFromFile("/summer-mis-en.properties");
        readFromFile("/summer-ui-en.properties");
    }

    @Override
    public String id() {
        return "en";
    }

    @Override
    public void readFromFile(String resourceFileName) {
        try {
            InputStream inputStream = Lang.class.getResourceAsStream(resourceFileName);
            if (inputStream != null) {
                Properties file = new Properties();
                file.load(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
                for (Object key : file.keySet()) {
                    buffer.put(key.toString(), new LanguageText(file.get(key).toString(), 0));
                }
            } else {
                log.warn("{} does not exist.", resourceFileName);
            }
        } catch (IOException e) {
            log.error("Failed to load the settings from the file: {}", resourceFileName);
        }
    }

    @Override
    public List<String> as(String group, List<String> textList) {
        List<String> result = new ArrayList<>();
        Set<String> waiting;
        lock.lock();
        try {
            waiting = asMap.get(group);
            if (waiting == null)
                waiting = new HashSet<>();
        } finally {
            lock.unlock();
        }
        for (String text : textList) {
            String key = text.length() > 32 ? MD5.get(text) : text;
            String bufferKey = String.join(".", group, key);
            LanguageText item = buffer.get(bufferKey);
            if (item == null) {
                result.add(text);
                waiting.add(text);
                continue;
            }
            // 从内存缓存中取出译文结果
            if (item.expire() == 0) {// 永不过期
                result.add(item.text());
            } else if (new Datetime(item.expire()).after(new Datetime())) {// 未过期
                result.add(item.text());
            } else {
                waiting.add(text);// 已过期 重新翻译
                result.add(item.text());
            }
        }
        if (!waiting.isEmpty()) {
            lock.lock();
            try {
                asMap.put(group, waiting);
            } finally {
                lock.unlock();
            }
        }
        return result;
    }

    @Override
    public Map<String, String> get(String group, Map<String, String> items) {
        Map<String, String> result = new LinkedHashMap<>();// 已经翻译的字典
        Map<String, String> waiting;
        lock.lock();
        try {
            waiting = getMap.get(group);
            if (waiting == null)
                waiting = new LinkedHashMap<>();// 已经翻译的字典
        } finally {
            lock.unlock();
        }
        for (String key : items.keySet()) {
            String defaultText = items.get(key);
            String bufferKey = String.join(".", group, key);
            LanguageText item = buffer.get(bufferKey);
            if (item == null) {
                waiting.put(key, defaultText);
                result.put(key, defaultText);
                continue;
            }
            // 从内存缓存中取出译文结果
            if (item.expire() == 0) // 永不过期
                result.put(key, item.text());
            else if (new Datetime(item.expire()).after(new Datetime())) // 未过期
                result.put(key, item.text());
            else {
                waiting.put(key, defaultText);// 过期重译
                result.put(key, item.text());
            }
        }
        if (!waiting.isEmpty()) {
            lock.lock();
            try {
                getMap.put(group, waiting);
            } finally {
                lock.unlock();
            }
        }
        return result;
    }

    @Override
    public Map<String, String> load(String group) {
        // 批量载入使用同步加载的方式
        KnowallTranslateLoad thread = new KnowallTranslateLoad(group, this.id());
        Map<String, LanguageText> languages = thread.translate();
        languages.forEach((k, v) -> {
            String buffKey = String.join(".", group, k);
            buffer.put(buffKey, v);
        });

        String prefix = group + ".";
        Map<String, String> result = new LinkedHashMap<>();
        buffer.forEach((k, v) -> {
            if (k.startsWith(prefix))
                result.put(k, v.text());
        });
        log.info("{} 批量加载，数据大小 {}", group, languages.size());
        return result;
    }

    @Scheduled(initialDelay = 5 * 1000, fixedDelay = 15 * 1000)
    private void translateAs() {
        lock.lock();
        try {
            asMap.entrySet().parallelStream().forEach(entry -> {
                String group = entry.getKey();
                Set<String> list = entry.getValue();
                if (!list.isEmpty()) {
                    log.info("开始翻译文本列表 {}，大小 {}", group, list.size());
                    KnowallTranslateAs thread = new KnowallTranslateAs(new ArrayList<>(list), this.id());
                    thread.setSourceLangId("zh");
                    CompletableFuture<Map<String, LanguageText>> future = CompletableFuture
                            .supplyAsync(thread::translate, executor);
                    future.thenAccept(languages -> languages.forEach((key, language) -> {
                        // 如果翻译的结果和原文本相同，则不存到buffer里面
                        if (!key.equals(language.text())) {
                            String bufferKey = String.join(".", group, key);
                            buffer.put(bufferKey, language);
                        } else {
                            log.warn("[{}] 翻译返回值和原值相等", key);
                        }
                    }));
                }
            });

            getMap.entrySet().parallelStream().forEach(entry -> {
                String group = entry.getKey();
                Map<String, String> items = entry.getValue();
                // 异步翻译并将结果写入缓存
                if (!items.isEmpty()) {
                    log.info("开始翻译组别列表 {}，大小 {}", group, items.size());
                    KnowallTranslateGet thread = new KnowallTranslateGet(group, items, this.id());
                    thread.setSourceLangId("zh");
                    CompletableFuture<Map<String, LanguageText>> future = CompletableFuture
                            .supplyAsync(thread::translate, executor);
                    future.thenAccept(languages -> languages.forEach((key, language) -> {
                        String bufferKey = String.join(".", group, key);
                        buffer.put(bufferKey, language);
                    }));
                }
            });

            // 翻译完成，重置列表
            asMap.clear();
            getMap.clear();
        } finally {
            lock.unlock();
        }
    }

}
