package cn.cerc.local.lang;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import cn.cerc.db.core.ILanguage;

@Component
public class LanguageCN implements ILanguage {

    @Override
    public String id() {
        return "cn";
    }

    @Override
    public List<String> as(String group, List<String> textList) {
        return textList;
    }

    @Override
    public Map<String, String> get(String group, Map<String, String> items) {
        return items;
    }

    @Override
    public Map<String, String> load(String group) {
        return Map.of();
    }

    @Override
    public void readFromFile(String resourceFileName) {

    }

}
