package cn.cerc.local.lang;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import cn.cerc.db.core.ILanguage;
import cn.cerc.db.core.LanguageFactory;
import cn.cerc.db.core.Utils;

@Component
public class LanguageFactoryImpl implements LanguageFactory, ApplicationContextAware {

    @Value("${server.language:cn}")
    private String languageId;
    private ApplicationContext applicationContext;

    @Override
    public ILanguage getLanguage() {
        if ("en".equals(languageId))
            return applicationContext.getBean(LanguageEN.class);
        else if ("tw".equals(languageId))
            return applicationContext.getBean(LanguageTW.class);
        else if ("vn".equals(languageId))
            return applicationContext.getBean(LanguageVN.class);
        else if (Utils.isNotEmpty(languageId) && !"cn".equals(languageId))
            return applicationContext.getBean(LanguageEN.class);
        else
            return applicationContext.getBean(LanguageCN.class);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

}
