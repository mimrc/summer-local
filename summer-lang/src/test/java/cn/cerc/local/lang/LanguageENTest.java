package cn.cerc.local.lang;

import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class LanguageENTest {

    private LanguageEN lang;

    @Before
    public void setup() {
        lang = new LanguageEN();
    }

    @Test
    public void test_as1() throws InterruptedException {
        for (int i = 0; i < 3; i++) {
            String result = lang.as("", List.of("打印机1")).get(0);
            String result2 = lang.as("", List.of("打印机2")).get(0);
            String result3 = lang.as("", List.of("打印机2")).get(0);
            Thread.sleep(1000);

            System.out.println("------ split ------");
            System.out.println(result);
            System.out.println(result2);
            System.out.println(result3);
        }
    }

    @Test
    public void test_as2() throws InterruptedException {
        for (int i = 0; i < 3; i++) {
            lang.as("", List.of("打印机1", "打印机2", "打印机2")).forEach(System.out::println);
            System.out.println("------ split ------");
            Thread.sleep(1000);
        }
    }

    @Test
    public void test_get() throws InterruptedException {
        for (int i = 0; i < 3; i++) {
            lang.get("menus", Map.of("FrmUserInfo", "用户资料维护", "FrmDept", "部门资料维护"))
                    .forEach((k, v) -> System.out.println(k + " -> " + v));
            System.out.println("------ split ------");
            Thread.sleep(1000);
        }
    }

    @Test
    public void test_load() throws InterruptedException {
        for (int i = 0; i < 3; i++) {
            lang.load("menus").forEach((k, v) -> System.out.println(k + " -> " + v));
            Thread.sleep(1000);
            System.out.println("------ split ------");
        }
    }

}