package cn.cerc.local.lang;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import cn.cerc.db.core.Datetime;
import cn.cerc.db.core.ILanguage;
import cn.cerc.db.core.Lang;
import cn.cerc.db.core.MD5;
import cn.cerc.ui.translate.KnowallTranslateAs;
import cn.cerc.ui.translate.KnowallTranslateGet;
import cn.cerc.ui.translate.KnowallTranslateLoad;
import cn.cerc.ui.translate.LanguageText;

@Component
public class LanguageTW implements ILanguage {
    private static final Logger log = LoggerFactory.getLogger(LanguageTW.class);
    private final Map<String, LanguageText> buffer = new ConcurrentHashMap<>();
    private final ExecutorService executor = Executors.newSingleThreadExecutor();

    public LanguageTW() {
        readFromFile("/summer-db-tw.properties");
        readFromFile("/summer-mis-tw.properties");
        readFromFile("/summer-ui-tw.properties");
    }

    @Override
    public String id() {
        return "tw";
    }

    @Override
    public void readFromFile(String resourceFileName) {
        try {
            InputStream inputStream = Lang.class.getResourceAsStream(resourceFileName);
            if (inputStream != null) {
                Properties file = new Properties();
                file.load(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
                for (Object key : file.keySet()) {
                    buffer.put(key.toString(), new LanguageText(file.get(key).toString(), 0));
                }
            } else {
                log.warn("{} does not exist.", resourceFileName);
            }
        } catch (IOException e) {
            log.error("Failed to load the settings from the file: {}", resourceFileName);
        }
    }

    @Override
    public List<String> as(String group, List<String> textList) {
        List<String> result = new ArrayList<>();
        List<String> waiting = new ArrayList<>();
        for (String text : textList) {
            String key = text.length() > 32 ? MD5.get(text) : text;
            String bufferKey = String.join(".", group, key);
            LanguageText item = buffer.get(bufferKey);
            if (item == null) {
                result.add(text);
                waiting.add(text);
                continue;
            }

            // 从内存缓存中取出译文结果
            if (item.expire() == 0) {// 永不过期
                result.add(item.text());
            } else if (new Datetime(item.expire()).after(new Datetime())) {// 未过期
                result.add(item.text());
            } else {
                waiting.add(text);// 已过期 重新翻译
                result.add(item.text());
            }
        }

        // 翻译已过期的文本
        if (!waiting.isEmpty()) {
            KnowallTranslateAs thread = new KnowallTranslateAs(waiting, this.id());
            thread.setSourceLangId("zh");
            CompletableFuture<Map<String, LanguageText>> future = CompletableFuture.supplyAsync(thread::translate,
                    executor);
            future.thenAccept(languages -> languages.forEach((key, language) -> {
                String bufferKey = String.join(".", group, key);
                buffer.put(bufferKey, language);
            }));
        }
        return result;
    }

    @Override
    public Map<String, String> get(String group, Map<String, String> items) {
        Map<String, String> result = new LinkedHashMap<>();// 已经翻译的字典
        Map<String, String> waiting = new LinkedHashMap<>();// 等待翻译的字典
        for (String key : items.keySet()) {
            String defaultText = items.get(key);
            String bufferKey = String.join(".", group, key);
            LanguageText item = buffer.get(bufferKey);
            if (item == null) {
                waiting.put(key, defaultText);
                result.put(bufferKey, defaultText);
                continue;
            }

            if (item.expire() == 0) // 永不过期
                result.put(key, item.text());
            else if (new Datetime(item.expire()).after(new Datetime())) // 未过期
                result.put(key, item.text());
            else {
                waiting.put(key, defaultText);// 过期重译
                result.put(key, item.text());
            }
        }

        // 异步翻译并将结果写入缓存
        if (!waiting.isEmpty()) {
            KnowallTranslateGet thread = new KnowallTranslateGet(group, waiting, this.id());
            thread.setSourceLangId("zh");
            CompletableFuture<Map<String, LanguageText>> future = CompletableFuture.supplyAsync(thread::translate,
                    executor);
            future.thenAccept(languages -> languages.forEach((key, language) -> {
                String bufferKey = String.join(".", group, key);
                buffer.put(bufferKey, language);
            }));
        }
        return result;
    }

    @Override
    public Map<String, String> load(String group) {
        // 批量载入使用同步加载的方式
        KnowallTranslateLoad thread = new KnowallTranslateLoad(group, this.id());
        Map<String, LanguageText> languages = thread.translate();
        languages.forEach((k, v) -> {
            String buffKey = String.join(".", group, k);
            buffer.put(buffKey, v);
        });

        String prefix = group + ".";
        Map<String, String> result = new LinkedHashMap<>();
        buffer.forEach((k, v) -> {
            if (k.startsWith(prefix))
                result.put(k, v.text());
        });
        return result;
    }

}
